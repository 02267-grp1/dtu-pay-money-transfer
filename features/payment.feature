Feature: Money Transfer

  Scenario: Money transfer sequence
    When a "RequestPayment" event is received
    Then event "PaymentResult" is sent
  