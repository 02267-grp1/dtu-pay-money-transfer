FROM eclipse-temurin:21-jre-alpine
COPY target/lib /usr/src/lib
COPY target/money-transfer-1.0-SNAPSHOT.jar /usr/src/
WORKDIR /usr/src/
CMD java -Xmx64m -jar money-transfer-1.0-SNAPSHOT.jar
