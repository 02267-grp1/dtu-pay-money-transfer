package dk.dtu;

import dk.dtu.models.Payment;
import dk.dtu.models.Token;
import fastmoney.BankService;
import fastmoney.BankServiceException_Exception;
import fastmoney.BankServiceService;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class PaymentService {

    private MessageQueue queue;

    private final BankService bankService = new BankServiceService().getBankServicePort();

    private CompletableFuture<Boolean> validToken;
    private CompletableFuture<String> merchantBankAccountId;
    private CompletableFuture<String> customerBankAccountId;
    private CompletableFuture<String> customerIdEvent;

    public PaymentService(MessageQueue queue) {
        this.queue = queue;
        this.queue.addHandler("RequestPayment", this::requestPayment);
        this.queue.addHandler("MerchantAccountResult", this::merchantAccountResult);
        this.queue.addHandler("TokenValidationResult", this::validateTokenResult);
        this.queue.addHandler("CustomerAccountIdResult", this::customerAccountIdResult);
        this.queue.addHandler("TokenToCustomerIdResult", this::tokenToCustomerIdResult);
    }
    /**
        @author s213555
     */
    public void merchantAccountResult(Event event) {
        String accountId = event.getArgument(0, String.class);
        merchantBankAccountId.complete(accountId);
    }
    /**
        @author s153277
     */
    public void validateTokenResult(Event event) {
        boolean result = event.getArgument(0, boolean.class);
        validToken.complete(result);
    }
    /**
        @author s222994
     */
    public void customerAccountIdResult(Event event) {
        String result = event.getArgument(0, String.class);
        customerBankAccountId.complete(result);
    }
    /**
        @author s222994
     */
    public void tokenToCustomerIdResult(Event event) {
        String customerId = event.getArgument(0, String.class);
        customerIdEvent.complete(customerId);
    }
    /**
        @author s222994
     */
    public void requestPayment(Event event) {
        Payment paymentObj = event.getArgument(0, Payment.class);
        startPayment(paymentObj.getAmount(), paymentObj.getCustomerToken(), paymentObj.getMerchantId());
    }
    /**
        @author s222994
     */
    private void startPayment(int amount, String customerToken, String merchantId) throws RuntimeException {
        merchantBankAccountId = new CompletableFuture<>();
        validToken = new CompletableFuture<>();
        customerIdEvent = new CompletableFuture<>();
        customerBankAccountId = new CompletableFuture<>();

        requestMerchantAccount(merchantId);
        String merchantAccountId = merchantBankAccountId.join();

        requestTokenToCustomerId(customerToken);
        UUID customerId = UUID.fromString(customerIdEvent.join());

        requestTokenValidation(customerToken, customerId);
        boolean tokenValid = validToken.join();

        requestCustomerBankAccountId(customerId);
        String customerAccountId = customerBankAccountId.join();

        if (!tokenValid) {
            throw new RuntimeException("Customer token " + customerToken + " is not valid");
        }

        String result;
        try {
            bankService.transferMoneyFromTo(customerAccountId, merchantAccountId, BigDecimal.valueOf(amount), "Payment");
            result = "Payment successful";
        } catch (BankServiceException_Exception e) {
            result = "Payment unsuccessful";
        }

        sendPaymentResult(result);
    }
    /**
        @author s194626
     */
    private void requestMerchantAccount(String merchantAccountId) {
        Event event = new Event("MerchantAccountRequested", new Object[]{UUID.fromString(merchantAccountId)});
        queue.publish(event);
    }
    /**
        @author s202082
     */
    private void requestTokenValidation(String customerToken, UUID customerId) {
        Token token = new Token(customerId.toString(), UUID.fromString(customerToken));
        Event event = new Event("TokenValidationRequested", new Object[]{token});
        queue.publish(event);
    }
    /**
        @author s202082
     */
    private void requestTokenToCustomerId(String customerToken) {
        Event event = new Event("TokenToCustomerIdRequested", new Object[]{customerToken});
        queue.publish(event);
    }
    /**
        @author s223142
     */
    private void requestCustomerBankAccountId(UUID customerId) {
        Event event = new Event("CustomerAccountIdRequested", new Object[]{customerId});
        queue.publish(event);
    }
    /**
        @author s223142
     */
    public void sendPaymentResult(String result) {
        Event event = new Event("PaymentResult", new Object[]{result});
        queue.publish(event);
    }
}
