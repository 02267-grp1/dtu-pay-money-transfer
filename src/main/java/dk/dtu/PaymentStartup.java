package dk.dtu;

import messaging.implementations.RabbitMqQueue;

public class PaymentStartup {
    public static void main(String[] args) throws Exception{
        new PaymentStartup().startUp();
    }

    private void startUp() throws Exception {
        System.out.println("startup");
        var mq = new RabbitMqQueue("rabbitmq");
        new PaymentService(mq);
    }
}
