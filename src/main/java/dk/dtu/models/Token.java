package dk.dtu.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.io.Serializable;
import java.util.UUID;

/**
    @author s222994
 */
@Data
@AllArgsConstructor
public class Token implements Serializable {
    private String customerId;
    private UUID uuid;
}